﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClickerGame;

namespace Level_Player_UnitTest
{
    [TestClass]
    public class LevelUnitTest
    {
        [TestMethod]
        public void TestLevelIfConstructor1WorksCorrectForPoints()
        {
            Level level = new Level();
            Assert.IsTrue(level.points == 0);
        }

        [TestMethod]
        public void TestLevelIfConstructor1WorksCorrectForNumber()
        {
            Level level = new Level();
            Assert.IsTrue(level.GetNumber() == 1);
        }
    }
}
