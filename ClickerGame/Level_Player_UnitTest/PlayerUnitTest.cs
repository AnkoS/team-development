﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClickerGame;

namespace Level_Player_UnitTest
{
    [TestClass]
    public class PlayerUnitTest
    {
        [TestMethod]
        public void TestPlayerIfConstructorWorksCorrect()
        {
            Player player = new Player();
            Assert.IsTrue(player.GetLifes() == 5);
        }

        [TestMethod]
        public void TestPlayerIfGoodClickWorksCorrect()
        {
            Player player = new Player();
            player.GoodClick(105);
            Assert.IsTrue(player.level.GetNumber() == 2);
        }

        [TestMethod]
        public void TestPlayerIfBadClickWorksCorrect()
        {
            Player player = new Player();
            player.BadClick(10);
            Assert.IsTrue(player.GetLifes() == 4);
        }
    }
}
