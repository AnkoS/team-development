﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;

namespace ClickerGame
{
    public class DataBase
    {
        public DataBase()
        {
            // строка подключения к БД
            connStr = "server=localhost;user=root;database=gameclickerdb;password=1234;";
            // создаём объект для подключения к БД
            conn = new MySqlConnection(connStr);
        }

        public void ConnOpen()
        {
            // устанавливаем соединение с БД
            conn.Open();
        }

        public void ConnClose()
        {
            // закрываем соединение с БД
            conn.Close();
        }

        // проверка свободы логина (есть ли он в таблице игроков)
        // true - есть
        // false - нет
        public bool CheckLogin(string login)
        {
            sql = "SELECT * FROM table_players WHERE login = '" + login + "'";
            command = new MySqlCommand(sql, conn);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                //Console.WriteLine("true");
                //Console.ReadKey();
                reader.Close();
                return true;
            }
            else
            {
                //Console.WriteLine("false");
                //Console.ReadKey();
                reader.Close();
                return false;
            }
        }

        // проверка наличия игрока в таблице игроков (совпадение логина и пароля)
        // true - есть
        // false - нет
        public bool CheckPlayer(string login, string password)
        {
            sql = "SELECT login, password FROM table_players WHERE login = '" + login + "' and password =" + password;
            command = new MySqlCommand(sql, conn);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                //Console.WriteLine("true");
                //Console.ReadKey();
                reader.Close();
                return true;
            }
            else
            {
                //Console.WriteLine("false");
                //Console.ReadKey();
                reader.Close();
                return false;
            }
        }

        // количество игроков в таблице игроков
        public int CountPlayers()
        {
            sql = "SELECT COUNT(*) FROM table_players";
            command = new MySqlCommand(sql, conn);
            string count = command.ExecuteScalar().ToString();
            // выводим ответ в консоль
            //Console.WriteLine(count);

            return Int32.Parse(count);
        }

        // получение массива логинов и массива паролей из таблицы игроков (использовать функцию CountPlayers для инициализации размеров передаваемых массивов)
        public void GetPlayers(string [] logins, string [] passwords)
        {
            sql = "SELECT * FROM table_players";
            command = new MySqlCommand(sql, conn);
            reader = command.ExecuteReader();
            int i = 0;

            while (reader.Read())
             {
                // элементы массива [] - это значения столбцов из запроса SELECT
                logins[i] = reader[0].ToString();
                passwords[i] = reader[1].ToString();
                i++;
             }
             reader.Close();
        }

        // получение массива 10 высших рекордов и массива игроков, которым они принадлежат
        public void GetRecords(string[] logins, string[] records)
        {
            sql = "SELECT * FROM table_records ORDER BY record limit 10";
            command = new MySqlCommand(sql, conn);
            reader = command.ExecuteReader();
            int i = 0;

            while (reader.Read())
            {
                // элементы массива [] - это значения столбцов из запроса SELECT
                logins[i] = reader[0].ToString();
                records[i] = reader[1].ToString();
                i++;
            }
            reader.Close();
        }

        // добавление нового игрока
        public void AddPlayer(string login, string password)
        {
            if (CheckLogin(login) == false)
            {
                sql = "INSERT INTO table_players(login, password) VALUES('" + login + "', '" + password + "')";
                command = new MySqlCommand(sql, conn);
                command.ExecuteNonQuery();
            }
        }

        // добавление нового рекорда
        public void AddRecord(string login, int record)
        {
            sql = "INSERT INTO table_records(login, record) VALUES('" + login + "', " + record + ")";
            command = new MySqlCommand(sql, conn);
            command.ExecuteNonQuery();
        }

        // удаление игрока по логину из таблицы игроков
        public void DeletePlayer(string login)
        {
            sql = "DELETE FROM table_players WHERE login ='"+login+"'";
            command = new MySqlCommand(sql, conn);
            command.ExecuteNonQuery();
        }

        // удаление рекорда по логину и рекорду из таблицы рекордов
        public void DeleteRecord(string login, int record)
        {
            sql = "DELETE FROM table_records WHERE login ='" + login + "' and record ="+record;
            command = new MySqlCommand(sql, conn);
            command.ExecuteNonQuery();
        }


        // строка подключения к БД
        private string connStr;
        // объект для подключения к БД
        private MySqlConnection conn;
        // запрос
        private string sql;
        // объект для выполнения SQL-запроса
        private MySqlCommand command;
        // объект для чтения ответа сервера
        private MySqlDataReader reader;
    }

    /*class Program
    {
        static void Main(string[] args)
        {
            DataBase db = new DataBase();
            db.ConnOpen();
            string[] logins = new string[db.CountPlayers()];
            string[] passwords = new string[db.CountPlayers()];
            // db.GetPlayers(logins,passwords);
            db.GetRecords(logins, passwords);
            db.ConnClose();
            Console.WriteLine(logins[0]);
            Console.WriteLine(passwords[0]);
            Console.ReadKey();
        }
    }*/
}