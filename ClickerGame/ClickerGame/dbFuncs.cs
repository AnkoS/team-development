﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;

namespace ClickerGame
{
    /// <summary>
    /// Data base class.
    /// ID string generated is "T:ClickerGame.DataBase".
    /// </summary>
    public class DataBase
    {
        /// <summary>
        /// Constructor for data base class.
        /// ID string generated is "M:ClickerGame.DataBase.#ctor".
        /// </summary>
        public DataBase()
        {
            // строка подключения к БД
            connStr = "server=localhost;user=root;database=gameclickerdb;password=1234;";
            // создаём объект для подключения к БД
            conn = new MySqlConnection(connStr);
        }

        /// <summary>
        /// Function which let user to open connection with data base.
        /// ID string generated is "M:ClickerGame.DataBase.ConnOpen".
        /// </summary>
        public void ConnOpen()
        {
            // устанавливаем соединение с БД
            conn.Open();
        }

        /// <summary>
        /// Function which close connection with data base.
        /// ID string generated is "M:ClickerGame.DataBase.ConnClose".
        /// </summary>
        public void ConnClose()
        {
            // закрываем соединение с БД
            conn.Close();
        }

        /// <summary>
        /// Function let user to check if the player with signed login is in table_players.
        /// ID string generated is "M:ClickerGame.DataBase.CheckLogin(string)".
        /// </summary>
        /// <param name="login">Login of player which must be found in table_players.</param>
        /// <returns>True - user with this login is in table_players. 
        /// False - no user with such login in table_players.
        /// </returns>
        public bool CheckLogin(string login)
        {
            sql = "SELECT * FROM table_players WHERE login = '" + login + "'";
            command = new MySqlCommand(sql, conn);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                //Console.WriteLine("true");
                //Console.ReadKey();
                reader.Close();
                return true;
            }
            else
            {
                //Console.WriteLine("false");
                //Console.ReadKey();
                reader.Close();
                return false;
            }
        }

        /// <summary>
        /// Function let user to check if the player with signed login and password is in table_players.
        /// ID string generated is "M:ClickerGame.DataBase.CheckPlayer(string,string)".
        /// </summary>
        /// <param name="login">Login of player which must be found in table_players.</param>
        /// <param name="password">Password of player which must be found in table_players.</param>
        /// <returns>True - player with such login and password is in table_players. 
        /// False - no player with such login and password in table_players.
        /// </returns>
        public bool CheckPlayer(string login, string password)
        {
            sql = "SELECT login, password FROM table_players WHERE login = '" + login + "' and password =" + password;
            command = new MySqlCommand(sql, conn);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                //Console.WriteLine("true");
                //Console.ReadKey();
                reader.Close();
                return true;
            }
            else
            {
                //Console.WriteLine("false");
                //Console.ReadKey();
                reader.Close();
                return false;
            }
        }


        /// <summary>
        /// Function calculates count of players in table_players.
        /// ID string generated is "M:ClickerGame.DataBase.CountPlayers".
        /// </summary>
        /// <returns>Int32.Parse(count) - Count of players in table_players.</returns>
        public int CountPlayers()
        {
            sql = "SELECT COUNT(*) FROM table_players";
            command = new MySqlCommand(sql, conn);
            string count = command.ExecuteScalar().ToString();
            // выводим ответ в консоль
            //Console.WriteLine(count);

            return Int32.Parse(count);
        }

        /// <summary>
        /// Function let user to get all players from table_players .
        /// ID string generated is "M:ClickerGame.DataBase.GetPlayers(string[],string[])".
        /// </summary>
        /// <param name="logins">Array of logins which will be filled.</param>
        /// <param name="passwords">Array of passwords which will be filled.</param>
        public void GetPlayers(string [] logins, string [] passwords)
        {
            sql = "SELECT * FROM table_players";
            command = new MySqlCommand(sql, conn);
            reader = command.ExecuteReader();
            int i = 0;

            while (reader.Read())
             {
                // элементы массива [] - это значения столбцов из запроса SELECT
                logins[i] = reader[0].ToString();
                passwords[i] = reader[1].ToString();
                i++;
             }
             reader.Close();
        }

        /// <summary>
        /// Function let user to get top 10 records from table_records.
        /// ID string generated is "M:ClickerGame.DataBase.GetRecords(string[],string[])".
        /// </summary>
        /// <param name="logins">Array of logins which will be filled.</param>
        /// <param name="records">Array of records which will be filled.</param>
        public void GetRecords(string[] logins, string[] records)
        {
            sql = "SELECT * FROM table_records ORDER BY record limit 10";
            command = new MySqlCommand(sql, conn);
            reader = command.ExecuteReader();
            int i = 0;

            while (reader.Read())
            {
                // элементы массива [] - это значения столбцов из запроса SELECT
                logins[i] = reader[0].ToString();
                records[i] = reader[1].ToString();
                i++;
            }
            reader.Close();
        }

        /// <summary>
        /// Function let user to add new player in table_players.
        /// ID string generated is "M:ClickerGame.DataBase.AddPlayer(string,string)".
        /// </summary>
        /// <param name="login">Login of adding player.</param>
        /// <param name="password">Password of adding player.</param>
        public void AddPlayer(string login, string password)
        {
            if (CheckLogin(login) == false)
            {
                sql = "INSERT INTO table_players(login, password) VALUES('" + login + "', '" + password + "')";
                command = new MySqlCommand(sql, conn);
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Function let user to add new record in table_records.
        /// ID string generated is "M:ClickerGame.DataBase.AddRecord(string,int)".
        /// </summary>
        /// <param name="login">Login of player which record must be added.</param>
        /// <param name="record">Record of player which must be added.</param>
        public void AddRecord(string login, int record)
        {
            sql = "INSERT INTO table_records(login, record) VALUES('" + login + "', " + record + ")";
            command = new MySqlCommand(sql, conn);
            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Function let user to delete player from table_players.
        /// ID string generated is "M:ClickerGame.DataBase.DeletePlayer(string)".
        /// </summary>
        /// <param name="login">Login of player which must be deleted.</param>
        public void DeletePlayer(string login)
        {
            sql = "DELETE FROM table_players WHERE login ='"+login+"'";
            command = new MySqlCommand(sql, conn);
            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Function let user to delete player with its record from table_records.
        /// ID string generated is "M:ClickerGame.DataBase.DeleteRecord(string,int)".
        /// </summary>
        /// <param name="login">Login of player which record must be deleted.</param>
        /// <param name="record">Record which must be deleted.</param>
        public void DeleteRecord(string login, int record)
        {
            sql = "DELETE FROM table_records WHERE login ='" + login + "' and record ="+record;
            command = new MySqlCommand(sql, conn);
            command.ExecuteNonQuery();
        }


        /// <summary>
        /// String of connection to data base
        /// ID string generated is "C:ClickerGame.DataBase.connStr".
        /// </summary>
        private string connStr;

        /// <summary>
        /// Object for connection to data base.
        /// ID string generated is "C:ClickerGame.DataBase.conn".
        /// </summary>
        private MySqlConnection conn;

        /// <summary>
        /// Param for query
        /// ID string generated is "C:ClickerGame.DataBase.sql".
        /// </summary>
        private string sql;

        /// <summary>
        /// Object for doing sql query.
        /// ID string generated is "C:ClickerGame.DataBase.command".
        /// </summary>
        private MySqlCommand command;

        /// <summary>
        /// Object for reading answer from server.
        /// ID string generated is "C:ClickerGame.DataBase.reader".
        /// </summary>
        private MySqlDataReader reader;
    }

    /*class Program
    {
        static void Main(string[] args)
        {
            DataBase db = new DataBase();
            db.ConnOpen();
            string[] logins = new string[db.CountPlayers()];
            string[] passwords = new string[db.CountPlayers()];
            // db.GetPlayers(logins,passwords);
            db.GetRecords(logins, passwords);
            db.ConnClose();
            Console.WriteLine(logins[0]);
            Console.WriteLine(passwords[0]);
            Console.ReadKey();
        }
    }*/
}