﻿namespace ClickerGame
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            this.EnteranceButton = new System.Windows.Forms.Button();
            this.QuitButton = new System.Windows.Forms.Button();
            this.AboutButton = new System.Windows.Forms.Button();
            this.RecordsButton = new System.Windows.Forms.Button();
            this.NewGameButton = new System.Windows.Forms.Button();
            this.MainMenuPanel = new System.Windows.Forms.Panel();
            this.FromLoginButton = new System.Windows.Forms.Button();
            this.LoginPanel = new System.Windows.Forms.Panel();
            this.PasswordTextBox = new System.Windows.Forms.TextBox();
            this.LoginTextBox = new System.Windows.Forms.TextBox();
            this.PasswordLabel = new System.Windows.Forms.Label();
            this.LoginLabel = new System.Windows.Forms.Label();
            this.VerifyAndLoginButton = new System.Windows.Forms.Button();
            this.RegistrationPanel = new System.Windows.Forms.Panel();
            this.RepeatPasswordLabel = new System.Windows.Forms.Label();
            this.RepeatPasswordTextBox = new System.Windows.Forms.TextBox();
            this.NewPasswordTextBox = new System.Windows.Forms.TextBox();
            this.NewLoginTextBox = new System.Windows.Forms.TextBox();
            this.NewPasswordLabel = new System.Windows.Forms.Label();
            this.NewLoginLabel = new System.Windows.Forms.Label();
            this.RegistrateButton = new System.Windows.Forms.Button();
            this.FromRegistrationButton = new System.Windows.Forms.Button();
            this.GamePanel = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LivesLabel = new System.Windows.Forms.Label();
            this.ResultLabel = new System.Windows.Forms.Label();
            this.LevelLabel = new System.Windows.Forms.Label();
            this.FromGameButton = new System.Windows.Forms.Button();
            this.GamePictureBox = new System.Windows.Forms.PictureBox();
            this.HighscoresPanel = new System.Windows.Forms.Panel();
            this.RecordScoresLabel = new System.Windows.Forms.Label();
            this.LoginRecordLabel = new System.Windows.Forms.Label();
            this.FromHighScoresButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.AboutPanel = new System.Windows.Forms.Panel();
            this.FromAboutButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.MainMenuPanel.SuspendLayout();
            this.LoginPanel.SuspendLayout();
            this.RegistrationPanel.SuspendLayout();
            this.GamePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GamePictureBox)).BeginInit();
            this.HighscoresPanel.SuspendLayout();
            this.AboutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // EnteranceButton
            // 
            this.EnteranceButton.AutoEllipsis = true;
            this.EnteranceButton.BackColor = System.Drawing.Color.AntiqueWhite;
            this.EnteranceButton.FlatAppearance.BorderSize = 0;
            this.EnteranceButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.EnteranceButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EnteranceButton.Location = new System.Drawing.Point(7, 114);
            this.EnteranceButton.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.EnteranceButton.Name = "EnteranceButton";
            this.EnteranceButton.Size = new System.Drawing.Size(310, 75);
            this.EnteranceButton.TabIndex = 9;
            this.EnteranceButton.Text = "Вход";
            this.EnteranceButton.UseVisualStyleBackColor = false;
            this.EnteranceButton.Click += new System.EventHandler(this.EnteranceButton_Click);
            // 
            // QuitButton
            // 
            this.QuitButton.BackColor = System.Drawing.Color.AntiqueWhite;
            this.QuitButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.QuitButton.FlatAppearance.BorderSize = 3;
            this.QuitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.QuitButton.Location = new System.Drawing.Point(7, 417);
            this.QuitButton.Name = "QuitButton";
            this.QuitButton.Size = new System.Drawing.Size(310, 75);
            this.QuitButton.TabIndex = 8;
            this.QuitButton.Text = "Выход";
            this.QuitButton.UseVisualStyleBackColor = false;
            this.QuitButton.Click += new System.EventHandler(this.QuitButton_Click);
            // 
            // AboutButton
            // 
            this.AboutButton.BackColor = System.Drawing.Color.AntiqueWhite;
            this.AboutButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AboutButton.Location = new System.Drawing.Point(7, 316);
            this.AboutButton.Name = "AboutButton";
            this.AboutButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.AboutButton.Size = new System.Drawing.Size(310, 75);
            this.AboutButton.TabIndex = 7;
            this.AboutButton.Text = "Об игре";
            this.AboutButton.UseVisualStyleBackColor = false;
            this.AboutButton.Click += new System.EventHandler(this.AboutButton_Click);
            // 
            // RecordsButton
            // 
            this.RecordsButton.BackColor = System.Drawing.Color.AntiqueWhite;
            this.RecordsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RecordsButton.Location = new System.Drawing.Point(7, 216);
            this.RecordsButton.Name = "RecordsButton";
            this.RecordsButton.Size = new System.Drawing.Size(310, 75);
            this.RecordsButton.TabIndex = 6;
            this.RecordsButton.Text = "Рекорды";
            this.RecordsButton.UseVisualStyleBackColor = false;
            this.RecordsButton.Click += new System.EventHandler(this.RecordsButton_Click);
            // 
            // NewGameButton
            // 
            this.NewGameButton.AutoEllipsis = true;
            this.NewGameButton.BackColor = System.Drawing.Color.AntiqueWhite;
            this.NewGameButton.FlatAppearance.BorderSize = 0;
            this.NewGameButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.NewGameButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NewGameButton.Location = new System.Drawing.Point(7, 10);
            this.NewGameButton.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.NewGameButton.Name = "NewGameButton";
            this.NewGameButton.Size = new System.Drawing.Size(310, 75);
            this.NewGameButton.TabIndex = 5;
            this.NewGameButton.Text = "Регистрация";
            this.NewGameButton.UseVisualStyleBackColor = false;
            this.NewGameButton.Click += new System.EventHandler(this.NewGameButton_Click);
            // 
            // MainMenuPanel
            // 
            this.MainMenuPanel.BackColor = System.Drawing.Color.Transparent;
            this.MainMenuPanel.Controls.Add(this.EnteranceButton);
            this.MainMenuPanel.Controls.Add(this.QuitButton);
            this.MainMenuPanel.Controls.Add(this.AboutButton);
            this.MainMenuPanel.Controls.Add(this.RecordsButton);
            this.MainMenuPanel.Controls.Add(this.NewGameButton);
            this.MainMenuPanel.Location = new System.Drawing.Point(413, 54);
            this.MainMenuPanel.Name = "MainMenuPanel";
            this.MainMenuPanel.Size = new System.Drawing.Size(327, 503);
            this.MainMenuPanel.TabIndex = 10;
            // 
            // FromLoginButton
            // 
            this.FromLoginButton.AutoEllipsis = true;
            this.FromLoginButton.BackColor = System.Drawing.Color.AntiqueWhite;
            this.FromLoginButton.FlatAppearance.BorderSize = 0;
            this.FromLoginButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.FromLoginButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FromLoginButton.Location = new System.Drawing.Point(6, 469);
            this.FromLoginButton.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.FromLoginButton.Name = "FromLoginButton";
            this.FromLoginButton.Size = new System.Drawing.Size(310, 75);
            this.FromLoginButton.TabIndex = 11;
            this.FromLoginButton.Text = "Назад";
            this.FromLoginButton.UseVisualStyleBackColor = false;
            this.FromLoginButton.Click += new System.EventHandler(this.FromLoginButton_Click);
            // 
            // LoginPanel
            // 
            this.LoginPanel.BackColor = System.Drawing.Color.Transparent;
            this.LoginPanel.Controls.Add(this.PasswordTextBox);
            this.LoginPanel.Controls.Add(this.LoginTextBox);
            this.LoginPanel.Controls.Add(this.PasswordLabel);
            this.LoginPanel.Controls.Add(this.LoginLabel);
            this.LoginPanel.Controls.Add(this.VerifyAndLoginButton);
            this.LoginPanel.Controls.Add(this.FromLoginButton);
            this.LoginPanel.Location = new System.Drawing.Point(13, 13);
            this.LoginPanel.Name = "LoginPanel";
            this.LoginPanel.Size = new System.Drawing.Size(1125, 585);
            this.LoginPanel.TabIndex = 12;
            this.LoginPanel.Visible = false;
            // 
            // PasswordTextBox
            // 
            this.PasswordTextBox.BackColor = System.Drawing.Color.AntiqueWhite;
            this.PasswordTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PasswordTextBox.Location = new System.Drawing.Point(400, 257);
            this.PasswordTextBox.MinimumSize = new System.Drawing.Size(75, 75);
            this.PasswordTextBox.Name = "PasswordTextBox";
            this.PasswordTextBox.PasswordChar = '*';
            this.PasswordTextBox.Size = new System.Drawing.Size(310, 48);
            this.PasswordTextBox.TabIndex = 16;
            // 
            // LoginTextBox
            // 
            this.LoginTextBox.BackColor = System.Drawing.Color.AntiqueWhite;
            this.LoginTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LoginTextBox.Location = new System.Drawing.Point(400, 101);
            this.LoginTextBox.MinimumSize = new System.Drawing.Size(75, 75);
            this.LoginTextBox.Name = "LoginTextBox";
            this.LoginTextBox.Size = new System.Drawing.Size(310, 48);
            this.LoginTextBox.TabIndex = 15;
            // 
            // PasswordLabel
            // 
            this.PasswordLabel.AutoSize = true;
            this.PasswordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PasswordLabel.ForeColor = System.Drawing.Color.Snow;
            this.PasswordLabel.Location = new System.Drawing.Point(400, 202);
            this.PasswordLabel.Name = "PasswordLabel";
            this.PasswordLabel.Size = new System.Drawing.Size(140, 37);
            this.PasswordLabel.TabIndex = 14;
            this.PasswordLabel.Text = "Пароль:";
            // 
            // LoginLabel
            // 
            this.LoginLabel.AutoSize = true;
            this.LoginLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LoginLabel.ForeColor = System.Drawing.Color.Snow;
            this.LoginLabel.Location = new System.Drawing.Point(400, 51);
            this.LoginLabel.Name = "LoginLabel";
            this.LoginLabel.Size = new System.Drawing.Size(113, 37);
            this.LoginLabel.TabIndex = 13;
            this.LoginLabel.Text = "Логин:";
            // 
            // VerifyAndLoginButton
            // 
            this.VerifyAndLoginButton.AutoEllipsis = true;
            this.VerifyAndLoginButton.BackColor = System.Drawing.Color.AntiqueWhite;
            this.VerifyAndLoginButton.FlatAppearance.BorderSize = 0;
            this.VerifyAndLoginButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.VerifyAndLoginButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.VerifyAndLoginButton.Location = new System.Drawing.Point(400, 377);
            this.VerifyAndLoginButton.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.VerifyAndLoginButton.Name = "VerifyAndLoginButton";
            this.VerifyAndLoginButton.Size = new System.Drawing.Size(310, 75);
            this.VerifyAndLoginButton.TabIndex = 12;
            this.VerifyAndLoginButton.Text = "Вход";
            this.VerifyAndLoginButton.UseVisualStyleBackColor = false;
            this.VerifyAndLoginButton.Click += new System.EventHandler(this.VerifyAndLoginButton_Click);
            // 
            // RegistrationPanel
            // 
            this.RegistrationPanel.BackColor = System.Drawing.Color.Transparent;
            this.RegistrationPanel.Controls.Add(this.RepeatPasswordLabel);
            this.RegistrationPanel.Controls.Add(this.RepeatPasswordTextBox);
            this.RegistrationPanel.Controls.Add(this.NewPasswordTextBox);
            this.RegistrationPanel.Controls.Add(this.NewLoginTextBox);
            this.RegistrationPanel.Controls.Add(this.NewPasswordLabel);
            this.RegistrationPanel.Controls.Add(this.NewLoginLabel);
            this.RegistrationPanel.Controls.Add(this.RegistrateButton);
            this.RegistrationPanel.Controls.Add(this.FromRegistrationButton);
            this.RegistrationPanel.Location = new System.Drawing.Point(13, 13);
            this.RegistrationPanel.Name = "RegistrationPanel";
            this.RegistrationPanel.Size = new System.Drawing.Size(1123, 579);
            this.RegistrationPanel.TabIndex = 17;
            this.RegistrationPanel.Visible = false;
            // 
            // RepeatPasswordLabel
            // 
            this.RepeatPasswordLabel.AutoSize = true;
            this.RepeatPasswordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RepeatPasswordLabel.ForeColor = System.Drawing.Color.Snow;
            this.RepeatPasswordLabel.Location = new System.Drawing.Point(400, 314);
            this.RepeatPasswordLabel.Name = "RepeatPasswordLabel";
            this.RepeatPasswordLabel.Size = new System.Drawing.Size(308, 36);
            this.RepeatPasswordLabel.TabIndex = 26;
            this.RepeatPasswordLabel.Text = "Повторите пароль:";
            // 
            // RepeatPasswordTextBox
            // 
            this.RepeatPasswordTextBox.BackColor = System.Drawing.Color.AntiqueWhite;
            this.RepeatPasswordTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RepeatPasswordTextBox.Location = new System.Drawing.Point(400, 365);
            this.RepeatPasswordTextBox.MinimumSize = new System.Drawing.Size(75, 75);
            this.RepeatPasswordTextBox.Name = "RepeatPasswordTextBox";
            this.RepeatPasswordTextBox.PasswordChar = '*';
            this.RepeatPasswordTextBox.Size = new System.Drawing.Size(310, 48);
            this.RepeatPasswordTextBox.TabIndex = 25;
            // 
            // NewPasswordTextBox
            // 
            this.NewPasswordTextBox.BackColor = System.Drawing.Color.AntiqueWhite;
            this.NewPasswordTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NewPasswordTextBox.Location = new System.Drawing.Point(400, 225);
            this.NewPasswordTextBox.MinimumSize = new System.Drawing.Size(75, 75);
            this.NewPasswordTextBox.Name = "NewPasswordTextBox";
            this.NewPasswordTextBox.PasswordChar = '*';
            this.NewPasswordTextBox.Size = new System.Drawing.Size(310, 48);
            this.NewPasswordTextBox.TabIndex = 24;
            // 
            // NewLoginTextBox
            // 
            this.NewLoginTextBox.BackColor = System.Drawing.Color.AntiqueWhite;
            this.NewLoginTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NewLoginTextBox.Location = new System.Drawing.Point(400, 91);
            this.NewLoginTextBox.MinimumSize = new System.Drawing.Size(75, 75);
            this.NewLoginTextBox.Name = "NewLoginTextBox";
            this.NewLoginTextBox.Size = new System.Drawing.Size(310, 48);
            this.NewLoginTextBox.TabIndex = 23;
            // 
            // NewPasswordLabel
            // 
            this.NewPasswordLabel.AutoSize = true;
            this.NewPasswordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NewPasswordLabel.ForeColor = System.Drawing.Color.Snow;
            this.NewPasswordLabel.Location = new System.Drawing.Point(400, 176);
            this.NewPasswordLabel.Name = "NewPasswordLabel";
            this.NewPasswordLabel.Size = new System.Drawing.Size(140, 37);
            this.NewPasswordLabel.TabIndex = 22;
            this.NewPasswordLabel.Text = "Пароль:";
            // 
            // NewLoginLabel
            // 
            this.NewLoginLabel.AutoSize = true;
            this.NewLoginLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NewLoginLabel.ForeColor = System.Drawing.Color.Snow;
            this.NewLoginLabel.Location = new System.Drawing.Point(400, 41);
            this.NewLoginLabel.Name = "NewLoginLabel";
            this.NewLoginLabel.Size = new System.Drawing.Size(113, 37);
            this.NewLoginLabel.TabIndex = 21;
            this.NewLoginLabel.Text = "Логин:";
            // 
            // RegistrateButton
            // 
            this.RegistrateButton.AutoEllipsis = true;
            this.RegistrateButton.BackColor = System.Drawing.Color.AntiqueWhite;
            this.RegistrateButton.FlatAppearance.BorderSize = 0;
            this.RegistrateButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.RegistrateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RegistrateButton.Location = new System.Drawing.Point(400, 469);
            this.RegistrateButton.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.RegistrateButton.Name = "RegistrateButton";
            this.RegistrateButton.Size = new System.Drawing.Size(310, 75);
            this.RegistrateButton.TabIndex = 20;
            this.RegistrateButton.Text = "Вход";
            this.RegistrateButton.UseVisualStyleBackColor = false;
            this.RegistrateButton.Click += new System.EventHandler(this.RegistrateButton_Click);
            // 
            // FromRegistrationButton
            // 
            this.FromRegistrationButton.AutoEllipsis = true;
            this.FromRegistrationButton.BackColor = System.Drawing.Color.AntiqueWhite;
            this.FromRegistrationButton.FlatAppearance.BorderSize = 0;
            this.FromRegistrationButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.FromRegistrationButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FromRegistrationButton.Location = new System.Drawing.Point(6, 469);
            this.FromRegistrationButton.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.FromRegistrationButton.Name = "FromRegistrationButton";
            this.FromRegistrationButton.Size = new System.Drawing.Size(310, 75);
            this.FromRegistrationButton.TabIndex = 19;
            this.FromRegistrationButton.Text = "Назад";
            this.FromRegistrationButton.UseVisualStyleBackColor = false;
            this.FromRegistrationButton.Click += new System.EventHandler(this.FromRegistrationButton_Click);
            // 
            // GamePanel
            // 
            this.GamePanel.BackColor = System.Drawing.Color.Transparent;
            this.GamePanel.Controls.Add(this.label4);
            this.GamePanel.Controls.Add(this.label3);
            this.GamePanel.Controls.Add(this.LivesLabel);
            this.GamePanel.Controls.Add(this.ResultLabel);
            this.GamePanel.Controls.Add(this.LevelLabel);
            this.GamePanel.Controls.Add(this.FromGameButton);
            this.GamePanel.Controls.Add(this.GamePictureBox);
            this.GamePanel.Location = new System.Drawing.Point(3, 2);
            this.GamePanel.Name = "GamePanel";
            this.GamePanel.Size = new System.Drawing.Size(1157, 615);
            this.GamePanel.TabIndex = 18;
            this.GamePanel.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(660, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 46);
            this.label4.TabIndex = 26;
            this.label4.Text = "Счёт";
            // 
            // label3
            // 
            this.label3.Cursor = System.Windows.Forms.Cursors.Cross;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 19F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
            this.label3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label3.Location = new System.Drawing.Point(979, 3);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label3.Size = new System.Drawing.Size(63, 57);
            this.label3.TabIndex = 25;
            this.label3.Text = " ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // LivesLabel
            // 
            this.LivesLabel.AutoSize = true;
            this.LivesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LivesLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.LivesLabel.Location = new System.Drawing.Point(1036, 8);
            this.LivesLabel.Name = "LivesLabel";
            this.LivesLabel.Size = new System.Drawing.Size(55, 46);
            this.LivesLabel.TabIndex = 24;
            this.LivesLabel.Text = "0 ";
            // 
            // ResultLabel
            // 
            this.ResultLabel.AutoSize = true;
            this.ResultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ResultLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ResultLabel.Location = new System.Drawing.Point(781, 3);
            this.ResultLabel.Name = "ResultLabel";
            this.ResultLabel.Size = new System.Drawing.Size(67, 46);
            this.ResultLabel.TabIndex = 22;
            this.ResultLabel.Text = " 0 ";
            // 
            // LevelLabel
            // 
            this.LevelLabel.AutoSize = true;
            this.LevelLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LevelLabel.ForeColor = System.Drawing.Color.Bisque;
            this.LevelLabel.Location = new System.Drawing.Point(303, 6);
            this.LevelLabel.Name = "LevelLabel";
            this.LevelLabel.Size = new System.Drawing.Size(220, 46);
            this.LevelLabel.TabIndex = 21;
            this.LevelLabel.Text = "Уровень 1";
            // 
            // FromGameButton
            // 
            this.FromGameButton.AutoEllipsis = true;
            this.FromGameButton.BackColor = System.Drawing.Color.AntiqueWhite;
            this.FromGameButton.FlatAppearance.BorderSize = 0;
            this.FromGameButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.FromGameButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FromGameButton.Location = new System.Drawing.Point(0, 3);
            this.FromGameButton.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.FromGameButton.Name = "FromGameButton";
            this.FromGameButton.Size = new System.Drawing.Size(162, 58);
            this.FromGameButton.TabIndex = 20;
            this.FromGameButton.Text = "Назад";
            this.FromGameButton.UseVisualStyleBackColor = false;
            this.FromGameButton.Click += new System.EventHandler(this.FromGameButton_Click);
            // 
            // GamePictureBox
            // 
            this.GamePictureBox.BackColor = System.Drawing.Color.Transparent;
            this.GamePictureBox.Location = new System.Drawing.Point(3, 63);
            this.GamePictureBox.Name = "GamePictureBox";
            this.GamePictureBox.Size = new System.Drawing.Size(1151, 554);
            this.GamePictureBox.TabIndex = 0;
            this.GamePictureBox.TabStop = false;
            // 
            // HighscoresPanel
            // 
            this.HighscoresPanel.BackColor = System.Drawing.Color.Transparent;
            this.HighscoresPanel.Controls.Add(this.RecordScoresLabel);
            this.HighscoresPanel.Controls.Add(this.LoginRecordLabel);
            this.HighscoresPanel.Controls.Add(this.FromHighScoresButton);
            this.HighscoresPanel.Controls.Add(this.label1);
            this.HighscoresPanel.Location = new System.Drawing.Point(14, 2);
            this.HighscoresPanel.Name = "HighscoresPanel";
            this.HighscoresPanel.Size = new System.Drawing.Size(1119, 596);
            this.HighscoresPanel.TabIndex = 27;
            this.HighscoresPanel.Visible = false;
            // 
            // RecordScoresLabel
            // 
            this.RecordScoresLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RecordScoresLabel.Location = new System.Drawing.Point(607, 89);
            this.RecordScoresLabel.Name = "RecordScoresLabel";
            this.RecordScoresLabel.Size = new System.Drawing.Size(403, 388);
            this.RecordScoresLabel.TabIndex = 22;
            this.RecordScoresLabel.Text = "Рекорды";
            // 
            // LoginRecordLabel
            // 
            this.LoginRecordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LoginRecordLabel.Location = new System.Drawing.Point(136, 89);
            this.LoginRecordLabel.Name = "LoginRecordLabel";
            this.LoginRecordLabel.Size = new System.Drawing.Size(403, 388);
            this.LoginRecordLabel.TabIndex = 21;
            this.LoginRecordLabel.Text = "Логин";
            // 
            // FromHighScoresButton
            // 
            this.FromHighScoresButton.AutoEllipsis = true;
            this.FromHighScoresButton.BackColor = System.Drawing.Color.AntiqueWhite;
            this.FromHighScoresButton.FlatAppearance.BorderSize = 0;
            this.FromHighScoresButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.FromHighScoresButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FromHighScoresButton.Location = new System.Drawing.Point(23, 496);
            this.FromHighScoresButton.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.FromHighScoresButton.Name = "FromHighScoresButton";
            this.FromHighScoresButton.Size = new System.Drawing.Size(310, 75);
            this.FromHighScoresButton.TabIndex = 20;
            this.FromHighScoresButton.Text = "Назад";
            this.FromHighScoresButton.UseVisualStyleBackColor = false;
            this.FromHighScoresButton.Click += new System.EventHandler(this.FromHighScoresButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(469, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(193, 46);
            this.label1.TabIndex = 1;
            this.label1.Text = "Рекорды";
            // 
            // AboutPanel
            // 
            this.AboutPanel.BackColor = System.Drawing.Color.Transparent;
            this.AboutPanel.Controls.Add(this.FromAboutButton);
            this.AboutPanel.Controls.Add(this.label2);
            this.AboutPanel.Location = new System.Drawing.Point(0, 12);
            this.AboutPanel.Name = "AboutPanel";
            this.AboutPanel.Size = new System.Drawing.Size(1138, 596);
            this.AboutPanel.TabIndex = 28;
            this.AboutPanel.Visible = false;
            // 
            // FromAboutButton
            // 
            this.FromAboutButton.AutoEllipsis = true;
            this.FromAboutButton.BackColor = System.Drawing.Color.AntiqueWhite;
            this.FromAboutButton.FlatAppearance.BorderSize = 0;
            this.FromAboutButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.FromAboutButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FromAboutButton.Location = new System.Drawing.Point(37, 472);
            this.FromAboutButton.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.FromAboutButton.Name = "FromAboutButton";
            this.FromAboutButton.Size = new System.Drawing.Size(310, 75);
            this.FromAboutButton.TabIndex = 21;
            this.FromAboutButton.Text = "Назад";
            this.FromAboutButton.UseVisualStyleBackColor = false;
            this.FromAboutButton.Click += new System.EventHandler(this.FromAboutButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label2.Location = new System.Drawing.Point(69, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 40);
            this.label2.TabIndex = 0;
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1165, 610);
            this.Controls.Add(this.GamePanel);
            this.Controls.Add(this.AboutPanel);
            this.Controls.Add(this.HighscoresPanel);
            this.Controls.Add(this.RegistrationPanel);
            this.Controls.Add(this.LoginPanel);
            this.Controls.Add(this.MainMenuPanel);
            this.Name = "MainMenu";
            this.Text = "MainMenu";
            this.MainMenuPanel.ResumeLayout(false);
            this.LoginPanel.ResumeLayout(false);
            this.LoginPanel.PerformLayout();
            this.RegistrationPanel.ResumeLayout(false);
            this.RegistrationPanel.PerformLayout();
            this.GamePanel.ResumeLayout(false);
            this.GamePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GamePictureBox)).EndInit();
            this.HighscoresPanel.ResumeLayout(false);
            this.HighscoresPanel.PerformLayout();
            this.AboutPanel.ResumeLayout(false);
            this.AboutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button EnteranceButton;
        private System.Windows.Forms.Button QuitButton;
        private System.Windows.Forms.Button AboutButton;
        private System.Windows.Forms.Button RecordsButton;
        private System.Windows.Forms.Button NewGameButton;
        private System.Windows.Forms.Panel MainMenuPanel;
        private System.Windows.Forms.Button FromLoginButton;
        private System.Windows.Forms.Panel LoginPanel;
        private System.Windows.Forms.Label LoginLabel;
        private System.Windows.Forms.Button VerifyAndLoginButton;
        private System.Windows.Forms.TextBox PasswordTextBox;
        private System.Windows.Forms.TextBox LoginTextBox;
        private System.Windows.Forms.Label PasswordLabel;
        private System.Windows.Forms.Panel RegistrationPanel;
        private System.Windows.Forms.Label RepeatPasswordLabel;
        private System.Windows.Forms.TextBox RepeatPasswordTextBox;
        private System.Windows.Forms.TextBox NewPasswordTextBox;
        private System.Windows.Forms.TextBox NewLoginTextBox;
        private System.Windows.Forms.Label NewPasswordLabel;
        private System.Windows.Forms.Label NewLoginLabel;
        private System.Windows.Forms.Button RegistrateButton;
        private System.Windows.Forms.Button FromRegistrationButton;
        private System.Windows.Forms.Panel GamePanel;
        private System.Windows.Forms.PictureBox GamePictureBox;
        private System.Windows.Forms.Label ResultLabel;
        private System.Windows.Forms.Label LevelLabel;
        private System.Windows.Forms.Button FromGameButton;
        private System.Windows.Forms.Panel HighscoresPanel;
        private System.Windows.Forms.Button FromHighScoresButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel AboutPanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button FromAboutButton;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label LoginRecordLabel;
        private System.Windows.Forms.Label RecordScoresLabel;
        private System.Windows.Forms.Label LivesLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}