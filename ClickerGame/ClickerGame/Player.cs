﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClickerGame
{
    /// <summary>
    /// This class describes events which happens to player.
    /// ID string generated is "T:ClickerGame.Player".
    /// </summary>
    public class Player
    {
        /// <summary>
        /// Player constructor.
        /// ID string generated is "M:ClickerGame.Player.#ctor".
        /// </summary>
        public Player()
        {
            lifes = 5;
            level = new Level();
        }

        /// <summary>
        /// Method deletes 1 life from player.
        /// ID string generated is "M:ClickerGame.Player.DeleteLife".
        /// </summary>
        /// <returns>True if player lose</returns>
        private bool DeleteLife()
        {
            if (lifes == 0)
            {
                return false;
            }
            else
            {
                lifes--;
                return true;
            }
        }

        /// <summary>
        /// Method describes event of clicking good target.
        /// ID string generated is "M:ClickerGame.Player.GoodClick(int)".
        /// </summary>
        /// <param name="points">Points which addes to current points of player</param>
        /// <returns>True if player leveled up</returns>
        public bool GoodClick(int points)
        {
            return level.AddPoints(points);
        }

        /// <summary>
        /// Method describes event of clicking bad target or missing good target.
        /// ID string generated is "M:ClickerGame.Player.BadClick(int)".
        /// </summary>
        /// <param name="points">Points which addes to current points of player</param>
        /// <returns>True if player lose</returns>
        public bool BadClick(int points)
        {
            level.RemovePoints(points);
            return !DeleteLife();
        }

        /// <summary>
        /// Method returns ammount of current lifes.
        /// ID string generated is "M:ClickerGame.Player.GetLifes".
        /// </summary>
        /// <returns>Ammount of current lifes</returns>
        public int GetLifes()
        {
            return lifes;
        }

        /// <summary>
        /// Current level of player
        /// ID string generated is "C:ClickerGame.Player.level".
        /// </summary>
        public Level level { set; get; }
        private int lifes { set; get; }
    }
}
