﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClickerGame
{
    /// <summary>
    /// This class describes events which happens to level of player.
    /// ID string generated is "T:ClickerGame.Level".
    /// </summary>
    public class Level
    {
        /// <summary>
        /// Level constructor.
        /// ID string generated is "M:ClickerGame.Level.#ctor()".
        /// </summary>
        public Level()
        {
            points = 0;
            number = 1;
        }

        /// <summary>
        /// Method adds points to current level.
        /// ID string generated is "M:ClickerGame.Level.AddPoints(int)".
        /// </summary>
        /// <param name="p">Additional points</param>
        /// <returns>True if player leveled up</returns>
        public bool AddPoints(int p)
        {
            points += p;
            if (nextLevel[number] <= points)
            {
                number++;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Method removes points from current level.
        /// ID string generated is "M:ClickerGame.Level.RemovePoints(int)".
        /// </summary>
        /// <param name="p">Points which removes</param>
        public void RemovePoints(int p)
        {
            if (nextLevel[number-1]>(points+p))
            {
                points = nextLevel[number - 1];
            }
            else
            {
                points += p;
            }
        }

        /// <summary>
        /// Method returns number of current level.
        /// ID string generated is "M:ClickerGame.Level.GetNumber".
        /// </summary>
        /// <returns>Number of current level</returns>
        public int GetNumber()
        {
            return number;
        }

        /// <summary>
        /// Current points of level.
        /// ID string generated is "C:ClickerGame.Level.points".
        /// </summary>
        public int points { set; get; }
        private int number { set; get; }
        private int[] nextLevel = new int[] { 0, 10, 20, 40, 70, 110, 160, 220, 290, 370, 500 };
    }
}
