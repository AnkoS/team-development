﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClickerGame
{
    /// <summary>
    /// Class for information messages.
    /// ID string generated is "T:ClickerGame.ErrorMessegeForm ".
    /// </summary>
    class ErrorMessegeForm : BasicForm
    {
        /// <summary>
        ///  Label that shows the message
        /// </summary>
        private System.Windows.Forms.Label ErrorLabel;

        /// <summary>
        ///  Button clicked for "Ok" answer
        /// </summary>
        private System.Windows.Forms.Button OkButton;

        /// <summary>
        /// ErrorMessageForm constructor.
        /// ID string generated is "M:ClickerGame.ErrorMessageForm.#ctor".
        /// </summary>
        public ErrorMessegeForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Methos to show dialog window
        /// ID string generated is "M:ClickerGame.ErrorMessageForm.ShowDialog(System.String)".
        /// </summary>
        /// <param name="str">The message to show.</param>
        /// <returns>Displays the form as a modal dialog box.</returns>
        public DialogResult ShowDialog (string str)
        {
            ErrorLabel.Text = str;
            return this.ShowDialog();
        }

        /// <summary>
        /// Method for initialazing components.
        /// ID string generated is "M:ClickerGame.ErrorMessageForm.InitializeComponent".
        /// </summary>
        private void InitializeComponent()
        {
            this.OkButton = new System.Windows.Forms.Button();
            this.ErrorLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // OkButton
            // 
            this.OkButton.AutoEllipsis = true;
            this.OkButton.BackColor = System.Drawing.Color.AntiqueWhite;
            this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OkButton.FlatAppearance.BorderSize = 0;
            this.OkButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.OkButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OkButton.Location = new System.Drawing.Point(121, 149);
            this.OkButton.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(162, 58);
            this.OkButton.TabIndex = 23;
            this.OkButton.Text = "ОК";
            this.OkButton.UseVisualStyleBackColor = false;
            // 
            // ErrorLabel
            // 
            this.ErrorLabel.BackColor = System.Drawing.Color.Transparent;
            this.ErrorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ErrorLabel.Location = new System.Drawing.Point(-1, 9);
            this.ErrorLabel.Name = "ErrorLabel";
            this.ErrorLabel.Size = new System.Drawing.Size(417, 137);
            this.ErrorLabel.TabIndex = 24;
            this.ErrorLabel.Text = "Ошибка";
            this.ErrorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ErrorMessegeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.ClientSize = new System.Drawing.Size(414, 240);
            this.Controls.Add(this.ErrorLabel);
            this.Controls.Add(this.OkButton);
            this.Name = "ErrorMessegeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.ResumeLayout(false);

        }
    }
}
