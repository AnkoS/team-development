﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClickerGame
{
    /// <summary>
    /// Class for bad targets.
    /// ID string generated is "T:ClickerGame.BadTarget".
    /// </summary>
    public class BadTarget : Target
    {
        /// <summary>
        /// BadTarget constructor.
        /// ID string generated is "M:ClickerGame.BadTarget.#ctor(int,int,int,Bitmap)".
        /// </summary>
        /// <param name="level">The level of the current game</param>
        /// <param name="canvasHeight">Height of the canvas to draw in</param>
        /// <param name="canvasWidth">Width of the canvas to draw in</param>
        /// <param name="image">Image of the target</param>
        public BadTarget(int level, int canvasHeight, int canvasWidth, Bitmap image)
            : base(level,canvasHeight, canvasWidth, image) { }

        /// <summary>
        /// Override method to click on bad target.
        /// ID string generated is "M:ClickerGame.BadTarget.Click".
        /// </summary>
        /// <returns>The weight to change the score with</returns>
        public override int Click()
        {
            return -(int)Weight;
        }

        /// <summary>
        /// Override method to miss-click on bad target.
        /// ID string generated is "M:ClickerGame.BadTarget.MissClick".
        /// </summary>
        /// <returns>The weight to change the score with</returns>
        public override int MissClick()
        {
            return 0;
        }
    }
}
