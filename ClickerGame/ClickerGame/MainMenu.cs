﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;

namespace ClickerGame
{
    /// <summary>
    /// Partial class that contains the GUI of the game
    /// ID string generated is "T:ClickerGame.MainMenu".
    /// </summary>
    public partial class MainMenu : BasicForm
    {
        /// <summary>
        /// Class MessageForm.
        /// </summary>
        private MessageForm messageForm = new MessageForm();
        /// <summary>
        /// Class ErrorMessegeForm.
        /// </summary>
        private ErrorMessegeForm errorMessageForm = new ErrorMessegeForm();
        /// <summary>
        /// Class Player.
        /// </summary>
        Player player;
        /// <summary>
        /// List of classes Target.
        /// </summary>
        List<Target> targets = new List<Target>();
        /// <summary>
        /// Enum describes events which happens after clicking pictureBox;
        /// </summary>
        enum resultClick { lose, lostLife, gainPoints, levelUp, won, nothing};
        /// <summary>
        /// Class DataBase.
        /// </summary>
        DataBase dataBase = new DataBase();
        /// <summary>
        ///  This string is for saving the name of the player.
        /// </summary>
        string playerLogin = "";
        /// <summary>
        /// Timer which invokes OnTimedEvent to move targets on the field
        /// </summary>
        System.Timers.Timer timer;
        /// <summary>
        /// This number is for next apearing of target
        /// </summary>
        int movesTillNextTarget = 0;
        /// <summary>
        /// Class Graphics.
        /// </summary>
        Graphics graphics;
        /// <summary>
        /// Class Bitmap.
        /// </summary>
        Bitmap bitmap;
        /// <summary>
        /// Needs for working with form from timer thread.
        /// </summary>
        delegate void MainWindow();

        /// <summary>
        /// MainMenu constructor.
        /// ID string generated is "M:ClickerGame.MainMenu.#ctor".
        /// </summary>
        public MainMenu()
        {
            InitializeComponent();
           // dataBase.ConnOpen();
            GamePictureBox.MouseClick += GamePictureBox_Click;
            bitmap = new Bitmap(GamePictureBox.Width, GamePictureBox.Height);
            GamePictureBox.Image = bitmap;
            graphics = Graphics.FromImage(bitmap);
        }
                
        /// <summary>
        /// Method resets point, level and lifes of player.
        /// ID string generated is "M:ClickerGame.MainMenu.NewGame".
        /// </summary>
        private void NewGame()
        {
            player = new Player();
        }

        /// <summary>
        /// Method creates timer.
        /// ID string generated is "M:ClickerGame.MainMenu.SetTimer".
        /// </summary>
        private void SetTimer()
        {
            timer = new System.Timers.Timer(60);
            timer.Elapsed += OnTimedEvent;
           // timer.AutoReset = true;
            timer.Enabled = true;
        }

        /// <summary>
        /// Method stops timer.
        /// ID string generated is "M:ClickerGame.MainMenu.StopTimer".
        /// </summary>
        private void StopTimer()
        {
            if (timer.Enabled)
            {
                timer.Enabled = false;
                // timer.AutoReset = false;
                timer.Elapsed -= OnTimedEvent;
            }
        }

        /// <summary>
        /// The event of the Timer. Adds new targets and redraws targets on the field.
        /// ID string generated is "M:ClickerGame.MainMenu.OnTimedEvent(System.Object, System.ElapsedEventArgs)".
        /// </summary>
        /// <param name="source">The source of the event.</param>
        /// <param name="e">An object that contains no event data.</param>
        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            timer.Enabled = false;
            int level = player.level.GetNumber();
            if (((movesTillNextTarget == 0) && (IsAllowedToGenerateNewTarget())) || (targets.Count == 0))
            {
                GenerateTarget(GamePictureBox.Width, GamePictureBox.Height);
                if (level < 3)
                    movesTillNextTarget = GamePictureBox.Width / targets[0].GetSpeed();
                else
                if (level < 5)
                    movesTillNextTarget = GamePictureBox.Width / targets[0].GetSpeed() / 2;
                else
                if (level < 8)
                    movesTillNextTarget = GamePictureBox.Width / targets[0].GetSpeed() / 3;
                else
                if (level > 7)
                    movesTillNextTarget = GamePictureBox.Width / targets[0].GetSpeed() / 4;
            }
            else
            if (movesTillNextTarget > 0)
            {
                if (movesTillNextTarget >= level)
                    movesTillNextTarget -= level;
                else
                    movesTillNextTarget = 0;
            }

            ClearScreen();           
            resultClick result = MoveTargets(GamePictureBox.Width);
            foreach (Target t in targets)
                AddTarget(t.Image, t.GetX(), t.GetY());

            MainWindow mainWindow;

            switch (result)
            {
                case resultClick.lose:
                    // dataBase.AddRecord(playerLogin, player.level.points);
                    mainWindow = SetGameOver;
                    this.Invoke(mainWindow);
                    break;
                case resultClick.lostLife:
                    mainWindow = SetLives;
                    LivesLabel.Invoke(mainWindow);
                    break;
            }

            timer.Enabled = true;          
        }

        /// <summary>
        /// Method sets the number of lives to LivesLabel.
        /// ID string generated is "M:ClickerGame.MainMenu.SetLives".
        /// </summary>
        private void SetLives()
        {
            ResultLabel.Text = player.level.points.ToString();
            LivesLabel.Text = player.GetLifes().ToString();
        }

        /// <summary>
        /// Method sets the number of level to LevelLabel.
        /// ID string generated is "M:ClickerGame.MainMenu.SetLives".
        /// </summary>
        private void SetLevel()
        {
            ResultLabel.Text = player.level.points.ToString();
            LevelLabel.Text = "Уровень " + player.level.GetNumber().ToString();
        }

        /// <summary>
        /// Method breaks the game and return player to main menu.
        /// ID string generated is "M:ClickerGame.MainMenu.SetGameOver".
        /// </summary>
        private void SetGameOver()
        {
            //  dataBase.AddRecord(playerLogin, player.level.points);
            ResultLabel.Text = player.level.points.ToString();
            StopTimer();
            errorMessageForm.ShowDialog(player.level.points.ToString() + " очков");
            GamePanel.Visible = false;
            MainMenuPanel.Visible = true;
        }

        /// <summary>
        /// Click event for good targets.
        /// ID string generated is "M:ClickerGame.MainMenu.GoodTargetClicked(int)".
        /// </summary>
        /// <param name="points">Additional points for player</param>
        /// <returns>Result of click</returns>
        private resultClick GoodTargetClicked(int points)
        {
            movesTillNextTarget = 2*player.level.GetNumber();
            if (player.GoodClick(points))
            {
                if (player.level.GetNumber() > 10)
                    return resultClick.won;
                for (int i = 0; i < targets.Count; i++)
                    targets[i].LevelUp();
                return resultClick.levelUp;
            }
            else
                return resultClick.gainPoints;
        }

        /// <summary>
        /// Click event for bad targets or event of missing good targets.
        /// ID string generated is "M:ClickerGame.MainMenu.BadClick(int)".
        /// </summary>
        /// <param name="badPoints">Retiring points for player</param>
        /// <returns>Result of missing or clicking target</returns>
        private resultClick BadClick(int badPoints)
        {
            if (player.BadClick(badPoints))
                return resultClick.lose;
            else
                return resultClick.lostLife;
        }

        /// <summary>
        /// Click event of player.
        /// ID string generated is "M:ClickerGame.MainMenu.PlayerClicked(int, int)".
        /// </summary>
        /// <param name="x">Coordinate X</param>
        /// <param name="y">Coordinate Y</param>
        /// <returns>Result of clicking</returns>
        private resultClick PlayerClicked(int x, int y) 
        {
            resultClick result = resultClick.nothing;
            int n = -1;
            for (int i = 0; i < targets.Count; i++)
            {
                if (targets[i].IsClicked(x, y))    
                {
                    n = i;
                    int points = targets[i].Click();
                    if (targets[i] is GoodTarget)                   
                    {
                        result = GoodTargetClicked(points);
                    }
                    else                                           
                    {
                        result =  BadClick(points);
                    }
                }
            }
            if (n >= 0)
                targets.Remove(targets[n]);

            int badPoints = 0;
            for (int i = 0; i < targets.Count; i++)
            {
                badPoints += targets[i].MissClick();
            }
            if (n == -1)
                result = BadClick(badPoints);

            return result;
        }

        /// <summary>
        /// Method calculates max number of targets on the field.
        /// ID string generated is "M:ClickerGame.MainMenu.GenerateNumberOfTarget".
        /// </summary>
        /// <returns>Number of max targets</returns>
        private int GenerateNumberOfTarget()
        {
            Random random = new Random();
            if (player.level.GetNumber() < 3)
                return 1;
            else
                if (player.level.GetNumber() < 5)
                    return random.Next(2) + 1;
                else
                    if (player.level.GetNumber() < 8)
                        return random.Next(3) + 1;
                    else
                        return random.Next(4) + 1;
        }

        /// <summary>
        /// Method calcilates probability of apearing of bad target.
        /// ID string generated is "M:ClickerGame.MainMenu.GetProbability".
        /// </summary>
        /// <returns>Probability of apearing of bad target</returns>
        private int GetProbability()
        {
            int level = player.level.GetNumber();
            if (level < 6)
            {
                return 15;
            }
            else
                if (level < 8)
            {
                return 25;
            }
            else
                    if (level < 10)
            {
                return 40;
            }
            else
            {
                return 60;
            }
        }

        /// <summary>
        /// Method checks is it allowed to create new target.
        /// ID string generated is "M:ClickerGame.MainMenu.IsAllowedToGenerateNewTarget".
        /// </summary>
        /// <returns>True - allowed, fakse - not allowed to create new target</returns>
        private bool IsAllowedToGenerateNewTarget()
        {
            int level = player.level.GetNumber();
            if (level < 3)
                return (targets.Count == 0);
            if (level < 5)
                return (targets.Count < 2);
            if (level < 8)
                return (targets.Count < 3);
            return (targets.Count < 4);
        }

        /// <summary>
        /// Method generates new target and addes it to list of targets.
        /// ID string generated is "M:ClickerGame.MainMenu.GenerateTarget(int, int)".
        /// </summary>
        /// <param name="canvasWidth">Width of game pannel</param>
        /// <param name="canvasHeight">Height of game pannel</param>
        private void GenerateTarget(int canvasWidth, int canvasHeight)
        {
            int number = GenerateNumberOfTarget();
            int level = player.level.GetNumber();
            String typeOfTarget = "good";
            if (level > 4)
            {
                Random random = new Random();
                int percent = random.Next(100);
                int probability = GetProbability();
                if (percent < probability)
                    typeOfTarget = "bad";
            }
            String name = typeOfTarget + number.ToString() + ".png";
            Bitmap bmp = new Bitmap(name);

            Target target;
            if (typeOfTarget == "good")
            {
                target = new GoodTarget(level, canvasHeight, canvasWidth, bmp);
            }
            else
            {
                target = new BadTarget(level, canvasHeight, canvasWidth, bmp);
            }
            targets.Add(target);
        }

        /// <summary>
        /// Method handles event of target becomes invisible.
        /// ID string generated is "M:ClickerGame.MainMenu.TargetNotVisible(Target)".
        /// </summary>
        /// <param name="target">Current target</param>
        /// <returns>Lose or lostLife if target was good, nothing if target was bad</returns>
        private resultClick TargetNotVisible(Target target)    
        {
            if (target is GoodTarget)
            {
                int points = target.MissClick();
                //player.BadClick(points);
                targets.Remove(target);
                return BadClick(points);
            }
            else
            {
                targets.Remove(target);
                return resultClick.nothing;
            }
        }

        /// <summary>
        /// Methid checks if target is still visible.
        /// ID string generated is "M:ClickerGame.MainMenu.IsTargetVisible(Target, int)".
        /// </summary>
        /// <param name="target">Verifiable target</param>
        /// <param name="canvasWidth">Width of game pannel</param>
        /// <returns>True if target is still visible, false if target is not visible</returns>
        private bool IsTargetVisible(Target target, int canvasWidth)
        {
            return (target.GetDirection() && (target.GetX() < canvasWidth)) ||
                (!target.GetDirection() && (target.GetX() > 0));
        }

        /// <summary>
        /// Method moves target on game pannel.
        /// ID string generated is "M:ClickerGame.MainMenu.MoveTarget(Target, int)".
        /// </summary>
        /// <param name="target">Current target</param>
        /// <param name="canvasWidth">Width of game pannel</param>
        /// <returns>Lose or lostLife if good target became invisible</returns>
        private resultClick MoveTarget(Target target, int canvasWidth)
        {
            Point p = target.Move();
            if (IsTargetVisible(target, canvasWidth))
            {
                return resultClick.nothing;
            }
            else
            {
                return TargetNotVisible(target);
            }
        }

        /// <summary>
        /// Method moves target on game pannel.
        /// ID string generated is "M:ClickerGame.MainMenu.MoveTarget(Target, int)".
        /// </summary>
        /// <param name="canvasWidth">Width of game pannel</param>
        /// <returns>Lose or lostLife if any of good targets became invisible</returns>
        private resultClick MoveTargets(int canvasWidth)
        {
            resultClick result = resultClick.nothing, tmp;
            for (int i = 0; i < targets.Count; i++)
            {
                tmp = MoveTarget(targets[i], canvasWidth);
                if (tmp != resultClick.nothing)
                    result = tmp;
            }
            return result;
        }
        
        /// <summary>
        /// The Click event of the QuitButton. Close the application.
        /// ID string generated is "M:ClickerGame.MainMenu.QuitButton_Click(System.Object, System.EventArgs)".
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An object that contains no event data.</param>
        private void QuitButton_Click(object sender, EventArgs e)
        {
           // dataBase.ConnClose();
            Close();
        }

        /// <summary>
        /// The Click event of the EnteranceButton. Shows the login form and hides main menu.
        /// ID string generated is "M:ClickerGame.MainMenu.EnteranceButton_Click(System.Object, System.EventArgs)".
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An object that contains no event data.</param>
        private void EnteranceButton_Click(object sender, EventArgs e)
        {
            MainMenuPanel.Visible = false;
            LoginPanel.Visible = true;           
        }

        /// <summary>
        /// The Click event of the FromLoginButton. Shows main menu and hides the login form.
        /// ID string generated is "M:ClickerGame.MainMenu.FromLoginButton_Click(System.Object, System.EventArgs)".
        /// </summary>     
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An object that contains no event data.</param>
        private void FromLoginButton_Click(object sender, EventArgs e)
        {
            DialogResult result = messageForm.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                LoginPanel.Visible = false;
                MainMenuPanel.Visible = true;
                LoginTextBox.Text = "";
                PasswordTextBox.Text = "";
            }
        }


        /// <summary>
        /// The Click event of the NewGameButton. Shows the registration form and hides main menu.
        /// ID string generated is "M:ClickerGame.MainMenu.NewGameButton_Click(System.Object, System.EventArgs)".
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An object that contains no event data.</param>
        private void NewGameButton_Click(object sender, EventArgs e)
        {
            MainMenuPanel.Visible = false;
            RegistrationPanel.Visible = true;
        }

        /// <summary>
        /// The Click event of the NewGameButton. Shows main menu and hides the registration form.
        /// ID string generated is "M:ClickerGame.MainMenu.NewGameButton_Click(System.Object, System.EventArgs)".
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An object that contains no event data.</param>
        private void FromRegistrationButton_Click(object sender, EventArgs e)
        { 
            DialogResult result = messageForm.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                RegistrationPanel.Visible = false;
                MainMenuPanel.Visible = true;
                NewLoginTextBox.Text = "";
                NewPasswordTextBox.Text = "";
                RepeatPasswordTextBox.Text = "";
            }
        }

        /// <summary>
        /// The Click event of the VerifyAndLoginButton. Cheсks the login and the password and starts the game if the the login and the password are match.
        /// ID string generated is "M:ClickerGame.MainMenu.VerifyAndLoginButton_Click(System.Object, System.EventArgs)".
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An object that contains no event data.</param>
        private void VerifyAndLoginButton_Click(object sender, EventArgs e)
        {
            string login = LoginTextBox.Text;
            string password = PasswordTextBox.Text;
            if (/*dataBase.CheckPlayer(login,password)*/ true)
            {
                playerLogin = LoginTextBox.Text;
                GamePanel.Visible = true;
                LoginPanel.Visible = false;
                LoginTextBox.Text = "";
                PasswordTextBox.Text = "";
                NewGame();
                SetLives();
                SetLevel();
                SetTimer();
            }
            else
            {
                errorMessageForm.ShowDialog("Неверный логин или пароль!");
                PasswordTextBox.Text = "";
            }
        }


        /// <summary>
        /// The Click event of the RegistrateButton. Try to registrate a new player and starts the game if registration is successful.
        /// ID string generated is "M:ClickerGame.MainMenu.RegistrateButton_Click(System.Object, System.EventArgs)".
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An object that contains no event data.</param>
        private void RegistrateButton_Click(object sender, EventArgs e)
        {
            if (!dataBase.CheckLogin(NewLoginTextBox.Text))
            {
                if (NewPasswordTextBox.Text == RepeatPasswordTextBox.Text)
                {
                    dataBase.AddPlayer(NewLoginTextBox.Text, NewPasswordTextBox.Text);
                    errorMessageForm.ShowDialog("Регистрация успешна!");
                    RegistrationPanel.Visible = false;
                    GamePanel.Visible = true;
                    NewGame();
                    SetLives();
                    LevelLabel.Text = player.level.GetNumber().ToString();
                    SetTimer();
                }
                else
                    errorMessageForm.ShowDialog("Пароли не совпадают!");
            }
            else
            {
                errorMessageForm.ShowDialog("Логин занят!");
            }
            playerLogin = NewLoginTextBox.Text;
            NewLoginTextBox.Text = "";
            NewPasswordTextBox.Text = "";
            RepeatPasswordTextBox.Text = "";
            
        }

        /// <summary>
        /// The Click event of the FromGameButton. Stops the game ans shows main menu.
        ///  ID string generated is "M:ClickerGame.MainMenu.FromGameButton_Click(System.Object, System.EventArgs)".
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An object that contains no event data.</param>
        private void FromGameButton_Click(object sender, EventArgs e)
        {
            StopTimer();
            DialogResult result = messageForm.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                SetGameOver();
            }
            else
                SetTimer();
        }

        /// <summary>
        /// The Click event of the FromHighScoresButton. Shows main menu and hides the high scores form.
        /// ID string generated is "M:ClickerGame.MainMenu.FromHighScoresButton_Click(System.Object, System.EventArgs)".
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An object that contains no event data.</param>
        private void FromHighScoresButton_Click(object sender, EventArgs e)
        {
            DialogResult result = messageForm.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                HighscoresPanel.Visible = false;
                MainMenuPanel.Visible = true;
                LoginRecordLabel.Text += "Логин:";
                RecordScoresLabel.Text += "Рекорд:";
            }
        }

        /// <summary>
        /// The Click event of the RecordsButton. Shows the high scores form and hides main menu.
        /// ID string generated is "M:ClickerGame.MainMenu.RecordsButton_Click(System.Object, System.EventArgs)".
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An object that contains no event data.</param>
        private void RecordsButton_Click(object sender, EventArgs e)
        {
            HighscoresPanel.Visible = true;
            MainMenuPanel.Visible = false;


            string[] logins = new string[dataBase.CountPlayers()];
            string[] records = new string[dataBase.CountPlayers()];
            dataBase.GetRecords(logins, records);
            int n = logins.Count();

            for (int i = 0; i < n; i++)
            {
                LoginRecordLabel.Text += "\n" + logins[i];
                RecordScoresLabel.Text += "\n" + records[i];
            }
           
        }

        /// <summary>
        /// The Click event of the FromAboutButton. Shows main menu and hides about form.
        /// ID string generated is "M:ClickerGame.MainMenu.FromAboutButton_Click(System.Object, System.EventArgs)".
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An object that contains no event data.</param>
        private void FromAboutButton_Click(object sender, EventArgs e)
        {
            DialogResult result = messageForm.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                AboutPanel.Visible = false;
                MainMenuPanel.Visible = true;
            }
        }

        /// <summary>
        /// The Click event of the AboutButton. Shows about form and hides main menu.
        /// ID string generated is "M:ClickerGame.MainMenu.AboutButton_Click(System.Object, System.EventArgs)".
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An object that contains no event data.</param>
        private void AboutButton_Click(object sender, EventArgs e)
        {
            AboutPanel.Visible = true;
            MainMenuPanel.Visible = false;
        }

        /// <summary>
        /// The Click event of the GamePictureBox. Shows about form and hides main menu.
        /// ID string generated is "M:ClickerGame.MainMenu.AboutButton_Click(System.Object, System.EventArgs)".
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An object that contains no event data.</param>
        private void GamePictureBox_Click(object sender, MouseEventArgs e)
        {
            switch (PlayerClicked(e.X, e.Y))
            {
                case resultClick.gainPoints:
                    ResultLabel.Text = player.level.points.ToString();
                    break;
                case resultClick.levelUp:
                    SetLevel();
                    break;
                case resultClick.lose:
                    SetGameOver();
                    break;
                case resultClick.lostLife:
                    SetLives();
                    break;
                case resultClick.won:
                    // dataBase.AddRecord(playerLogin, player.level.points);
                    StopTimer();
                    ResultLabel.Text = player.level.points.ToString();
                    errorMessageForm.ShowDialog("Победа !" + player.level.points.ToString() + " очков");
                    GamePanel.Visible = false;
                    MainMenuPanel.Visible = true;
                    break;
            }
        }


        /// <summary>
        /// Method for drawing targets on the picturebox.
        /// ID string generated is "M:ClickerGame.MainMenu.AddTarget(System.Drawing.PictureBox, System.Int32,System.Int32)".
        /// </summary>
        /// <param name="pic">The Bitmap of picture.</param>
        /// <param name="x">X coordinate of the place to draw.</param>
        /// <param name="y">Y coordinate of the place to draw.</param>
        private void AddTarget(Bitmap pic, int x, int y)
        {
            graphics.DrawImage(pic, new Point(x, y));        
        }

        /// <summary>
        /// Method for clearing the picturebox.
        /// ID string generated is "M:ClickerGame.MainMenu.ClearScreen".
        /// </summary>
        private void ClearScreen()
        {         
            graphics.Clear(Color.Transparent);
            GamePictureBox.Invalidate();
        }
    }
}
