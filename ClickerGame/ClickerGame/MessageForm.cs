﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace ClickerGame
{
    /// <summary>
    /// Class for Yes/No questions.
    /// ID string generated is "T:ClickerGame.MessageForm ".
    /// </summary>
    class MessageForm : BasicForm
    {
        /// <summary>
        ///  Button clicked for "No" answer
        /// </summary>
        private System.Windows.Forms.Button CancelButton;

        /// <summary>
        ///  Button clicked for "Yes" answer
        /// </summary>
        private System.Windows.Forms.Button OkButton;

        /// <summary>
        ///  Label that shows the question
        /// </summary>
        private System.Windows.Forms.Label QuestionLabel;

        /// <summary>
        /// MessageForm constructor.
        /// ID string generated is "M:ClickerGame.MessageForm.#ctor".
        /// </summary>
        public MessageForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// MessageForm constructor.
        /// ID string generated is "M:ClickerGame.MessageForm.#ctor(System.String)".
        /// </summary>
        /// <param name="str">The question to show</param>
        public MessageForm(string str)
        {
            InitializeComponent();
            QuestionLabel.Text = str;
        }

        /// <summary>
        /// Method for initialazing components.
        /// ID string generated is "M:ClickerGame.MessageForm.InitializeComponent".
        /// </summary>
        private void InitializeComponent()
        {
            this.CancelButton = new System.Windows.Forms.Button();
            this.OkButton = new System.Windows.Forms.Button();
            this.QuestionLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // CancelButton
            // 
            this.CancelButton.AutoEllipsis = true;
            this.CancelButton.BackColor = System.Drawing.Color.AntiqueWhite;
            this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.No;
            this.CancelButton.FlatAppearance.BorderSize = 0;
            this.CancelButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.CancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CancelButton.Location = new System.Drawing.Point(15, 140);
            this.CancelButton.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(162, 58);
            this.CancelButton.TabIndex = 21;
            this.CancelButton.Text = "Нет";
            this.CancelButton.UseVisualStyleBackColor = false;
            // 
            // OkButton
            // 
            this.OkButton.AutoEllipsis = true;
            this.OkButton.BackColor = System.Drawing.Color.AntiqueWhite;
            this.OkButton.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.OkButton.FlatAppearance.BorderSize = 0;
            this.OkButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.OkButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OkButton.Location = new System.Drawing.Point(228, 140);
            this.OkButton.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(162, 58);
            this.OkButton.TabIndex = 22;
            this.OkButton.Text = "Да";
            this.OkButton.UseVisualStyleBackColor = false;
            // 
            // QuestionLabel
            // 
            this.QuestionLabel.AutoSize = true;
            this.QuestionLabel.BackColor = System.Drawing.Color.Transparent;
            this.QuestionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.QuestionLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.QuestionLabel.Location = new System.Drawing.Point(29, 53);
            this.QuestionLabel.Name = "QuestionLabel";
            this.QuestionLabel.Size = new System.Drawing.Size(361, 40);
            this.QuestionLabel.TabIndex = 23;
            this.QuestionLabel.Text = "Вернуться в меню?";
            this.QuestionLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // MessageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.ClientSize = new System.Drawing.Size(414, 240);
            this.Controls.Add(this.QuestionLabel);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.CancelButton);
            this.Name = "MessageForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

       
    }
}
