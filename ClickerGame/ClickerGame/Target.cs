﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClickerGame
{
    /// <summary>
    /// Abstract class for moving targets.
    /// ID string generated is "T:ClickerGame.Target".
    /// </summary>
    public abstract class Target
    {
        /// <summary>
        /// Target constructor.
        /// ID string generated is "M:ClickerGame.Target.#ctor(int,int,int,Bitmap)".
        /// </summary>
        /// <param name="level">The level of the current game</param>
        /// <param name="canvasHeight">Height of the canvas to draw in</param>
        /// <param name="canvasWidth">Width of the canvas to draw in</param>
        /// <param name="image">Image of the target</param>
        public Target(int level, int canvasHeight, int canvasWidth, Bitmap image)
        {
            Speed = level; //change is needed
            Weight = (uint)level;//change is needed

            Random random = new Random();
            Image = new Bitmap(image);
            /// Example inside
            if (random.Next(2) == 0)
            {
                Direction = true;
                X = 0;
            }
            else
            {
                Direction = false;
                X = canvasWidth;
            }
            Y = random.Next(canvasHeight - Image.Height);
        }

        /// <summary>
        /// Abstract method for clicking on target.
        /// ID string generated is "M:ClickerGame.Target.Click".
        /// </summary>
        /// <returns>Value to change the score with</returns>
        public abstract int Click();

        /// <summary>
        /// Abstract method for target miss-clicking.
        /// ID string generated is "M:ClickerGame.Target.MissClick".
        /// </summary>
        /// <returns>Value to change the score with</returns>
        public abstract int MissClick();

        /// <summary>
        /// Increase the speed when level is up.
        /// ID string generated is "M:ClickerGame.Target.LevelUp".
        /// </summary>
        public void LevelUp()
        {
            Speed++;
        }

        /// <summary>
        /// To know was target clicked or not.
        /// ID string generated is "M:ClickerGame.Target.IsClicked(int, int)".
        /// </summary>
        /// <param name="x">Coordinate X of click on the canvas</param>
        /// <param name="y">Coordinate Y of click on the canvas</param>
        /// <returns>True if clicked, false otherwise</returns>
        public bool IsClicked(int x, int y)
        {
            if ((x >= X && x <= X + Image.Width) &&
                (y >= Y && y <= Y + Image.Height))
                return true;
            return false;
        }

        /// <summary>
        /// To get new coordinates when target moved.
        /// ID string generated is "M:ClickerGame.Target.Move".
        /// </summary>
        /// <returns>New coordinates of target</returns>
        public Point Move()
        {
            if (Direction)
                X += Speed;
            else
                X -= Speed;
            return new Point(X, Y);
        }

        /// <summary>
        /// To get the speed of target.
        /// ID string generated is "M:ClickerGame.Target.GetSpeed".
        /// </summary>
        /// <returns>Target speed</returns>
        public int GetSpeed()
        {
            return Speed;
        }

        /// <summary>
        /// To get the coordinate X of target.
        /// ID string generated is "M:ClickerGame.Target.GetX".
        /// </summary>
        /// <returns>Target X coordinate</returns>
        public int GetX()
        {
            return X;
        }

        /// <summary>
        /// To get the weight of target.
        /// ID string generated is "M:ClickerGame.Target.GetWeight".
        /// </summary>
        /// <returns>Target weight</returns>
        public uint GetWeight()
        {
            return Weight;
        }

        /// <summary>
        /// To get the Y coordinate of target.
        /// ID string generated is "M:ClickerGame.Target.GetY".
        /// </summary>
        /// <returns>Target Y coordinate</returns>
        public int GetY()
        {
            return Y;
        }

        /// <summary>
        /// To get the direction of moving target.
        /// ID string generated is "M:ClickerGame.Target.GetDirection".
        /// </summary>
        /// <returns>Target direction of moving</returns>
        public bool GetDirection()
        {
            return Direction;
        }

        /// <summary>
        /// The speed of target to move
        /// ID string generated is "C:ClickerGame.Target.Speed".
        /// </summary>
        protected int Speed { set; get; }

        /// <summary>
        /// The value of target to change the player score.
        /// ID string generated is "C:ClickerGame.Target.Weight".
        /// </summary>
        protected uint Weight { set; get; }

        /// <summary>
        /// The image of target to draw on canvas.
        /// ID string generated is "C:ClickerGame.Target.Image".
        /// </summary>
        public Bitmap Image { set; get; }

        /// <summary>
        /// Coordinate X of target on the canvas.
        /// ID string generated is "C:ClickerGame.Target.X".
        /// </summary>
        protected int X { set; get; }

        /// <summary>
        /// Coordinate Y of target on the canvas.
        /// ID string generated is "C:ClickerGame.Target.Y".
        /// </summary>
        protected int Y { set; get; }

        /// <summary>
        /// The target direction to move.
        /// true is to move right.
        /// ID string generated is "C:ClickerGame.Target.Direction".
        /// </summary>
        protected bool Direction { set; get; }
        
        
    }
}
