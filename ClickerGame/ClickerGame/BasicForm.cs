﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClickerGame
{
    /// <summary>
    /// Partial class with basic settings for all forms in project.
    /// ID string generated is "T:ClickerGame.BasicForm".
    /// </summary>
    public partial class BasicForm : Form
    {
        /// <summary>
        /// BasicForm constructor.
        /// ID string generated is "M:ClickerGame.BasicForm.ctor".
        /// </summary>
        public BasicForm()
        {
            InitializeComponent();
        }
    }
}
