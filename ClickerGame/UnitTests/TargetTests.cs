﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClickerGame;
using System.Drawing;

namespace UnitTests
{
    [TestClass]
    public class TargetTests
    {
        [TestMethod]
        public void TestBadTargetConstructor()
        {
            int level = 1;
            int canvasHeight = 1000;
            int canvasWidth = 1000;
            Bitmap bitmap = new Bitmap(100,100);
            BadTarget badTarget = new BadTarget(level,canvasHeight, canvasWidth, bitmap);
            Assert.IsNotNull(badTarget);
        }

        [TestMethod]
        public void TestBadTargetConstructorImage()
        {
            int level = 1;
            int canvasHeight = 1000;
            int canvasWidth = 1000;
            Bitmap bitmap = new Bitmap(100, 100);
            BadTarget badTarget = new BadTarget(level, canvasHeight, canvasWidth, bitmap);
            Assert.IsNotNull(badTarget.Image);
        }

        [TestMethod]
        public void TestBadTargetConstructorWeight()
        {
            int level = 1;
            int canvasHeight = 1000;
            int canvasWidth = 1000;
            Bitmap bitmap = new Bitmap(100, 100);
            BadTarget badTarget = new BadTarget(level, canvasHeight, canvasWidth, bitmap);
            Assert.IsTrue(badTarget.GetWeight()==1);
        }

        [TestMethod]
        public void TestBadTargetConstructorDirection()
        {
            int level = 1;
            int canvasHeight = 1000;
            int canvasWidth = 1000;
            Bitmap bitmap = new Bitmap(100, 100);
            BadTarget badTarget = new BadTarget(level, canvasHeight, canvasWidth, bitmap);
            Assert.IsTrue(badTarget.GetDirection() == true || badTarget.GetDirection() == false);
        }

        [TestMethod]
        public void TestBadTargetConstructorX()
        {
            int level = 1;
            int canvasHeight = 1000;
            int canvasWidth = 1000;
            Bitmap bitmap = new Bitmap(100, 100);
            BadTarget badTarget = new BadTarget(level, canvasHeight, canvasWidth, bitmap);
            Assert.IsTrue((badTarget.GetDirection() && badTarget.GetX() == 0) ||
                (!badTarget.GetDirection() && badTarget.GetX() == 1000));
        }

        [TestMethod]
        public void TestBadTargetConstructorY()
        {
            int level = 1;
            int canvasHeight = 1000;
            int canvasWidth = 1000;
            Bitmap bitmap = new Bitmap(100, 100);
            BadTarget badTarget = new BadTarget(level, canvasHeight, canvasWidth, bitmap);
            Assert.IsTrue(badTarget.GetY() < (canvasHeight - bitmap.Height));
        }

        [TestMethod]
        public void TestBadTargetConstructorSpeed()
        {
            int level = 1;
            int canvasHeight = 1000;
            int canvasWidth = 1000;
            Bitmap bitmap = new Bitmap(100, 100);
            BadTarget badTarget = new BadTarget(level, canvasHeight, canvasWidth, bitmap);
            Assert.IsTrue(badTarget.GetSpeed() == 1);
        }

        [TestMethod]
        public void TestBadTargetClick()
        {
            int level = 1;
            int canvasHeight = 1000;
            int canvasWidth = 1000;
            Bitmap bitmap = new Bitmap(100, 100);
            BadTarget badTarget = new BadTarget(level, canvasHeight, canvasWidth, bitmap);
            Assert.IsTrue(badTarget.Click() == -1);
        }

        [TestMethod]
        public void TestBadTargetMissClick()
        {
            int level = 1;
            int canvasHeight = 1000;
            int canvasWidth = 1000;
            Bitmap bitmap = new Bitmap(100, 100);
            BadTarget badTarget = new BadTarget(level, canvasHeight, canvasWidth, bitmap);
            Assert.IsTrue(badTarget.MissClick() == 0);
        }
    }
}
